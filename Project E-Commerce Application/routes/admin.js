var express = require('express');
var router = express.Router();

const productHelpers = require('../helpers/product-helpers')                 // To call the addProduct:(product) function

/* GET users listing. */
router.get('/', function(req, res, next) {
  productHelpers.getAllProducts().then((products)=>{                       // PROMISE FUNCTION
    console.log(products)
    res.render('admin/view-products',{products, admin:true})
  })
  
})
router.get('/add-product', function(req,res){
  res.render('admin/add-product',{admin:true})
})

router.post('/add-product',(req,res)=>{
  productHelpers.addProduct(req.body,(id)=>{
    let image = req.files.Image
    console.log(id)
    image.mv('./public/product-images/'+id+'.jpg',(err,done)=>{
      if(!err){
        res.render("admin/add-product")
      }else{
        console.log(err)
      }
    })
  })
})

router.get('/delete-product/:id',(req,res)=>{
    let prodId=req.params.id
    console.log(prodId);
    productHelpers.deleteProduct(prodId).then((response)=>{
      res.redirect('/admin/')
    })
})

router.get('/edit-product/:id',async (req,res)=>{
  let product=await productHelpers.getProductDetails(req.params.id)
  res.render('admin/edit-product',{product})
})

router.post('/edit-product/:id',(req,res)=>{
  console.log(req.params.id)
  let id=req.params.id
  productHelpers.updateProduct(req.params.id,req.body).then(()=>{
    res.redirect('/admin')
    if(req.files.Image){
      let image=req.files.Image
      image.mv('./public/product-images/'+id+'.jpg')
    }
  })
})

module.exports = router;
