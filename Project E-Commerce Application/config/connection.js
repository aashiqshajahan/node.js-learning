// DATABASE CONNECT CHEYYAANULLA FILE

const mongoClient=require('mongodb').MongoClient
const state={                                           //  ~ db ennum parnjh object aayit connecteyyaananu state enn ondaakkiyath
    db:null
}

module.exports.connect=function(done){                  // Ethu fileil ninn vilichaalum connection kittan
    const url='mongodb://localhost:27017'               // Oru mongodb connecteyyan url venam, database name venam
    const dbname='shopping'

    mongoClient.connect(url,(err,data)=>{               // Creating connection... 
        if(err) return done(err)
        state.db = data.db(dbname)                      // Connection vannu, ini connectionte koode database peru cherthitt database edkanam
        done()
    })

    
}

module.exports.get = function(){                       // Database kittan vendiyitt ulla function 
    return state.db
}