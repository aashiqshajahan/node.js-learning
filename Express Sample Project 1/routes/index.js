var express = require('express');
var router = express.Router();
var path = require('path');
var MongoClient = require('mongodb').MongoClient

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Sign Up' });
});

router.post('/submit', function(req,res){
 
  MongoClient.connect('mongodb://localhost:27017',function(err,client){
    if(err)                                                     // To check whether the database is connected.
      console.log('error')
    else  
      client.db('testdb').collection('user').insertOne(req.body)
  })

  res.send('Submitted Successfully')
})


module.exports = router;
