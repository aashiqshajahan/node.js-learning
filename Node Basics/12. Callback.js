var date = new Date()
console.log("HELLO")
for(i=0;i<=1;i++){
    console.log("LOOP")
}
console.log("END LOOP")
var diff = new Date()-date
console.log(diff)

console.log("Sample Function for call back using Synchronous")
console.log("-----------------------------------------------")


function longtask(millisecond){
    date = new Date()
    while((new Date - date) <= millisecond){
    
    }
}
console.log("Started")
longtask(2000)
console.log("End")

console.log("Started")
longtask(2000)
console.log("End")

console.log("Sample function for call back using Asynchronous")
console.log("-------------------------------------------------")

function showEnd(){
    console.log("End")
}

console.log("Started")
setTimeout(showEnd,2000)

console.log("Started")
setTimeout(showEnd,1000)

console.log("Started")
setTimeout(showEnd,3000)

console.log("Creating an asynchronous callback function")
console.log("------------------------------------------")

var hello = function(name){
    console.log("Name: "+ name)
}

var hai = function(callbackk){
    callbackk("Aashiq Shajahan")
}

hai(hello)

module.exports.MyName = function(){
    console.log("My Name is Aashiq Shajahan")
    console.log("This is printed from the file name 12. Callback ")
}
