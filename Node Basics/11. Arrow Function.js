//In normal Case:

function add(num1,num2){
    return num1+num2
}

console.log(add(32,1))

//By using Arrow Function 

let addition = (num1,num2) => num1+num2
console.log(addition(322,11))

//or

let subtraction = (num1,num2) => {
    console.log("Difference between two numbers: ")
    return num1-num2
}

console.log(subtraction(3335,2))
