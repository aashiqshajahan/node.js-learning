function Person(Name, Age, Place){
    this.Name = Name
    this.Age = Age
    this.Place = Place
    this.display = function(){
        console.log("Name: " + this.Name  + "\nAge: " + this.Age + "\nPlace: " + this.Place)

    }
}

var Aashiq = new Person("Aashiq ", 21, " Manacaudu")
var Aasif = new Person("Aasif ", 27, " Kallattumukku")

Aashiq.display()
Aasif.display()

var date = new Date("29 November 1999")
var date2 = new Date("31 August 2020 12:37 AM")
console.log(date.getDate())
console.log(date.getDay())
console.log(date2.getDay())
console.log(date2.getMinutes())
    