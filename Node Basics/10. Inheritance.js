class sampleclass{
    sampleconstructor(){
        console.log("Hi, I am a constructor of the class named sampleclass")
    }
}

class originalclass extends sampleclass{
    originalconstructor(){
        console.log("Hey, I am the original constructor of the original class")  
    }
}


let newObjectofOriginalClass = new originalclass()
newObjectofOriginalClass.sampleconstructor()

//let objectofsampleclass = new sampleclass()
 //objectofsampleclass.sampleconstructor()

