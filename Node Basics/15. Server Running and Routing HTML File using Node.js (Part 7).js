//Server creation and adding an html file
//---------------------------------------
var http = require("http")
var fs = require("fs")                                          // fs module to read html file
var url = require('url')                                        // url - module related with "url.parse" & q.pathname


// const { type } = require("os")
// const { url } = require("inspector")

http.createServer(function(req,res){

    var q = url.parse(req.url,true)                             // url.parse - Ith vech aa url ine capture cheyyaan pattum || true is to get the response as object
    // console.log(q.pathname)

    if(q.pathname==='/'){                                       // '/' means host maathram ollath i.e eg: facebook.com

    fs.readFile("sampleform.html",function(err,data){

        res.writeHead(200, {"Content-Type":"text/html"})        // Server request cheyyumbol successfull aayi enn kaanikkunna standard code aanu 200
        res.write(data)
        res.end()
    })

}else if(q.pathname==="/signupaction"){                          // sampleform.html il form action //signuppaction aanuu kodthekkunnath, athil GET method aayond adikkunna detail queryil kittum
    res.writeHead(200, {"Content-Type":"text/html"})             // Ith undenkile thaazhe res.write() il html aayit ezhthaan pattolu
    // console.log(q.query)                                      // To print the submitted queries in terminal
    res.write('<h1>Message Sent Successfully</h1><br><h2>Name: '+q.query.name+'</h2><h3>If Your First Name is shown, You can confirm that your response is recorded.</h3>')
    res.end()

}

else if(req.url==="/login2"){
    
    fs.readFile("student_regform.html",function(err,data){
        res.writeHead(200, {"Content-Type":"text/html"})
        res.write(data)
        res.end()
    })
        
}else if(req.url==="/login"){
    res.write("Login Page is under Construction")
    res.end()
}else{
    res.writeHead(404,{"Content-Type":"text/html"})             // Work cheyyunnilla enn kaanikkunnathinte oru normal format
    res.write("Error")
    res.end()
}
}).listen(8000, () => console.log("Server Running"))            // "Server Running" ennath Terminalil ezthi kaanikkanam, enkil athinu vendi ulla function(Arrow Function is used here)
