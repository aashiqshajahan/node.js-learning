var http = require("http")

http.createServer(server).listen(3333)

function server(req,res){
    res.write("This is a sample server 3333.")
    res.end()
}

http.createServer(function(req,res){
    res.write("This server is created by another form of server creation")
    res.end()
}).listen(3339)
