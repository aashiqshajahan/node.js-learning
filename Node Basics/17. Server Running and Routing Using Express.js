const bodyParser = require('body-parser');
const { urlencoded } = require('body-parser');
var { response } = require('express');                          // respone constant aakan paadilla
const express = require('express')
const path = require('path')



const PORT = 8002;
const app = express()
var urlencodedParser = bodyParser.urlencoded({extended: false})
app.use(express.static('public'))

// app.use(function(req,res,next){                                 // next -- next enna call back function use cheythillenkil request kodkumbo avde thanne stop aakum
//     console.log('Start')
//     next()
// })

app.get('/',function(req,res){                             // Get method is necessary, To request the url,             
    res.sendFile(path.join(__dirname,'sampleform.html'))        // We have to give the correct directory for the sendFile --> To check the current directory try console.log(__dirname)
    //console.log('Middle')                                       // Work cheyyunnundo enn check cheyyan
    //next()      
})


app.post('/signup',urlencodedParser,function(req,res){
    response = {
        Name:req.body.name,
        Email:req.body.email,
        Subject:req.body.subject,
        Message:req.body.message
    };
    console.log(response)
    res.end(JSON.stringify(response))
})                                                       // app.post works only if app.get is used/called.
    

 
app.listen(PORT,function(){
    // console.log(__dirname)
    console.log('Server Running on port:' +PORT);

})
