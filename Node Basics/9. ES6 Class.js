class Hello{
    constructor(num1,num2){
        this.num1=num1
        this.num2=num2
    }

    functionofHello(){          //Here, inside a class there is no need to add function keyword for declaring a function
        console.log("Sum of the two numbers: " + (this.num1+this.num2))
    }        

}

let nameoftheobject= new Hello(30,303)
nameoftheobject.functionofHello()

