var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

    const values = ["Aashiq","Aasif","Arshed","Shajahan"];

    const person = {name: "Aashiq", comments:{comment:"This is a sample comment", date: "23-05-2020"}}
    
    const security = {name: "Aashiq", admin:(false)}

  res.render('test', {security});
});

module.exports = router;
